from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .forms import Friend_Form
from .models import Friend
from .views import index, add_friend

# Create your tests here.
class AddFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/friends/')
		self.assertEqual(response.status_code, 200)

	def test_add_friend_using_index_func(self):
		found = resolve('/friends/')
		self.assertEqual(found.func, index)

	def test_models_can_create_new_friend(self):
		new_activity = Friend.objects.create(name='Aldi',url='http://aldialdoy.herokuapp.com')
		counting_all_available_friend=Friend.objects.all().count()
		self.assertEqual(counting_all_available_friend,1)

	def test_form_message_input_has_placeholder_and_css_classes(self):
		form = Friend_Form()
		self.assertIn('class="form-control"', form.as_p())
		self.assertIn('<label for="id_name">Name:</label>', form.as_p())
		self.assertIn('<label for="id_url">URL:</label>', form.as_p())
		
	def test_form_validation_for_blank_items(self):
		form = Friend_Form(data={'name': '', 'url': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
		form.errors['url'],
		["This field is required."]
		)

	def test_add_friend_addfriend_url_exist(self):
		response = Client().get('/friends/add_friend/')
		self.assertEqual(response.status_code, 200)

	def test_add_friend_addfriend_using_add_friend_func(self):
		found = resolve('/friends/add_friend/')
		self.assertEqual(found.func, add_friend)

	def test_addfriend_post_success_and_render_the_result(self):
		test = ''
		response_post = Client().post('/friends/add_friend/', {'name': test, 'url': test})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/friends/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_addfriend_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/friends/add_friend/', {'name': '', 'url': ''})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/friends/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_friend_showing_all_messages(self):
		name_omin = 'Omin'
		url_omin = 'omin.cikarang.co'
		data_omin = {'name': name_omin, 'url': url_omin}
		post_data_omin = Client().post('/friends/add_friend', data_omin)
		self.assertEqual(post_data_omin.status_code, 200)

		response = Client().get('/friends/add_friend')
		html_response = response.content.decode('utf8')

		for key,data in data_omin.items():
		 	self.assertIn(data,html_response)

		self.assertIn('Omin', html_response)
		self.assertIn(url_omin, html_response)
