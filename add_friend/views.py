from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

# Create your views here.
response = {}
def index(request):
	response['friend_form'] = Friend_Form
	response['friend'] = Friend.objects.all()
	html = 'friend_list/friend_list.html'
	return render(request, html, response)
	
def add_friend(request):
	form = Friend_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name']
		response['url'] = request.POST['url']
		friend = Friend(name=form.cleaned_data['name'], url=form.cleaned_data['url'])
		friend.save()
	else:
		response['form'] = form
		
	html = 'friend_list/friend_list.html'
	response['friend'] = Friend.objects.all()
	return render(request, html, response)