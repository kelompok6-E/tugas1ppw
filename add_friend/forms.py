from django import forms

class Friend_Form(forms.Form):

    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan url herokuapp',
   }
    name_attrs = {
        'class': 'form-control',
        'placeholder':'Your friend\'s name'
    }

    url_attrs = {
        'class': 'form-control',
        'placeholder':'Your friend\'s url'
    }
    
    name = forms.CharField(label='Name',required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label="URL",required=True, widget=forms.URLInput(attrs=url_attrs))