from django.shortcuts import render
from .models import DataProfile

name = 'Hepzibah Smith'
birth = '01 Jan'
gender = 'Female'
expertise = 'Marketing | Collector | Public Speaking'
description = 'Antique expert. Experience as marketer for 10 years'
email = 'hello@smith.com'
profile = DataProfile(nama = name, birthday = birth, gender = gender, expertise = expertise, description = description, email = email)

def index(request):
    response = {'name' : profile.nama, 'birthday' : profile.birthday, 'gender' : profile.gender, 'expertise' : profile.expertise, 'description' : profile.description, 'email' : profile.email}
    return render(request, 'halprofil/hal_profil.html', response)