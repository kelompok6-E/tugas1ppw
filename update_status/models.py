from django.db import models

# Create your models here.
class Status(models.Model):
    status = models.TextField(max_length=140)
    date = models.DateTimeField(auto_now_add=True)
