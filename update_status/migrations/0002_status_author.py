# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-12 03:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('update_status', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='author',
            field=models.TextField(default='Hapzibah Smith', max_length=30),
            preserve_default=False,
        ),
    ]
