from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from datetime import datetime
from .forms import Status_Form
from .models import Status
from halprofil.views import name

# Create your views here.
status_dict = {}

def index(request):    
    status_dict['author'] = name
    status = Status.objects.all()
    status_dict['status'] = status
    html = 'update_status/update_status.html'
    status_dict['status_form'] = Status_Form
    return render(request, html, status_dict)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        status_dict['status_form'] = request.POST['status_form']
        status = Status(status=status_dict['status_form'])
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')