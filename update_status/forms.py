from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Please tell us what are you thinking',
    }
    status_attrs = {
        'type': 'text',
        'cols': 180,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'What are you thinking...'
    }

    status_form = forms.CharField(label='', required=True, max_length=140, widget=forms.Textarea(attrs=status_attrs))
